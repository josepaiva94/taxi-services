

-- query: number of trips started per day of the week
SELECT dt_init.day_name, sum(no_trips)
FROM service
  LEFT JOIN datetime AS dt_init ON service.datetime_init_id = dt_init.datetime_id
GROUP BY dt_init.day_name;

-- query: number of trips started per day of the week and type of day
SELECT dt_init.day_name, dt_init.day_type, sum(no_trips)
FROM service
  LEFT JOIN datetime AS dt_init ON service.datetime_init_id = dt_init.datetime_id
GROUP BY dt_init.day_name, dt_init.day_type;

-- query: number of trips started per day of the week and type of day with totals
SELECT dt_init.day_abrv, dt_init.day_type, sum(no_trips)
FROM service
  LEFT JOIN datetime AS dt_init ON service.datetime_init_id = dt_init.datetime_id
GROUP BY CUBE(dt_init.day_abrv, dt_init.day_type);

-- query: average number of trips started per day of the week and type of day with totals
SELECT day_abrv, day_type, avg(sum.no_trips)
FROM (
  SELECT day_abrv, day_type, sum(no_trips) AS no_trips
  FROM service
    LEFT JOIN datetime AS dt_init ON service.datetime_init_id = dt_init.datetime_id
  GROUP BY day_month, month_current, year_current, day_abrv, day_type) AS sum
GROUP BY CUBE(day_abrv, day_type);

-- query: 5 locations of max services starting and services of that locations
SELECT location.freguesia AS freguesia, count(*) AS services_count
FROM service
  INNER JOIN location ON service.local_init_id = location.location_id
GROUP BY location.freguesia
ORDER BY services_count DESC
LIMIT 5;

-- query: 5 locations of max services ending and services of that locations
SELECT location.freguesia AS freguesia, count(*) AS services_count
FROM service
  INNER JOIN location ON service.local_end_id = location.location_id
GROUP BY location.freguesia
ORDER BY services_count DESC
LIMIT 5;

-- query: 5 locations of min services starting and services of that locations
SELECT location.freguesia AS freguesia, count(*) AS services_count
FROM service
  INNER JOIN location ON service.local_init_id = location.location_id
GROUP BY location.freguesia
ORDER BY services_count ASC
LIMIT 5;

-- query: 5 locations of min services ending and services of that locations
SELECT location.freguesia AS freguesia, count(*) AS services_count
FROM service
  INNER JOIN location ON service.local_end_id = location.location_id
GROUP BY location.freguesia
ORDER BY services_count ASC
LIMIT 5;

-- query: 10 taxis with more trips
SELECT taxi.license_nr AS license_nr, sum(no_trips) AS total_trips
FROM service INNER JOIN taxi ON service.taxi_id = taxi.taxi_id
GROUP BY taxi.license_nr
ORDER BY total_trips DESC
LIMIT 10;

-- number of trips started per month
SELECT dt_init.month_name, sum(no_trips)
FROM service
  LEFT JOIN datetime AS dt_init ON service.datetime_init_id = dt_init.datetime_id
GROUP BY dt_init.month_name;

-- query: taxis with less than 10 hours of work in the month with maximum work time
SELECT taxi.license_nr
FROM service INNER JOIN taxi ON service.taxi_id = taxi.taxi_id
  INNER JOIN datetime ON service.datetime_init_id = datetime.datetime_id
  INNER JOIN (
               SELECT datetime.month_name AS month_name, sum(total_time) AS month_total_time
               FROM service INNER JOIN datetime ON service.datetime_init_id = datetime.datetime_id
               GROUP BY datetime.month_name
               ORDER BY month_total_time DESC
               LIMIT 1
             ) AS max_month_total_time ON datetime.month_name = max_month_total_time.month_name
GROUP BY taxi.license_nr
HAVING sum(total_time) < 10 * 60 * 60;

-- query: for taxi with license number 80 check from which stand it starts the trip by day of the week
SELECT license_nr, day_name, stand.name, sum(no_trips) AS no_trips
FROM service
  INNER JOIN taxi ON service.taxi_id = taxi.taxi_id
  INNER JOIN datetime ON service.datetime_init_id = datetime.datetime_id
  INNER JOIN location ON service.local_init_id = location.location_id
  INNER JOIN stand_location ON location.location_id = stand_location.location_id
  INNER JOIN stand ON stand_location.stand_id = stand.stand_id
GROUP BY CUBE(license_nr, day_name, stand.name)
HAVING license_nr = 80;

-- query: for taxi with license number 80 check from which stand it starts the trip by hour
SELECT license_nr, hour, stand.name, sum(no_trips) AS no_trips
FROM service
  INNER JOIN taxi ON service.taxi_id = taxi.taxi_id
  INNER JOIN datetime ON service.datetime_init_id = datetime.datetime_id
  INNER JOIN location ON service.local_init_id = location.location_id
  INNER JOIN stand_location ON location.location_id = stand_location.location_id
  INNER JOIN stand ON stand_location.stand_id = stand.stand_id
GROUP BY CUBE(license_nr, hour, stand.name)
HAVING license_nr = 80;

-- query: number of taxis per stand
SELECT stand.name, count(DISTINCT taxi.license_nr) AS nr_taxis
FROM service
  INNER JOIN taxi ON service.taxi_id = taxi.taxi_id
  INNER JOIN location ON service.local_init_id = location.location_id
  INNER JOIN stand_location ON location.location_id = stand_location.location_id
  INNER JOIN stand ON stand_location.stand_id = stand.stand_id
GROUP BY stand.name;

-- query: number of services starting at each freguesia
SELECT freguesia, count(*) AS nr_services
FROM service
  INNER JOIN location ON service.local_init_id = location.location_id
GROUP BY freguesia;

-- query: CUBE BY initial location, final location and taxi license
SELECT initial_loc.freguesia AS initial_freguesia, final_loc.freguesia AS final_freguesia, taxi.license_nr, avg(total_time)
FROM service
  LEFT JOIN location AS initial_loc ON service.local_init_id = initial_loc.location_id
  LEFT JOIN location AS final_loc ON service.local_end_id = final_loc.location_id
  LEFT JOIN taxi ON service.taxi_id = taxi.taxi_id
WHERE initial_loc.location_id <> final_loc.location_id
GROUP BY CUBE(final_loc.freguesia, initial_loc.freguesia, taxi.license_nr);

-- query: select number of services at 4h intervals
SELECT hour / 4, count(*)
FROM service INNER JOIN datetime ON service.datetime_init_id = datetime.datetime_id
GROUP BY 1
ORDER BY 1;
