
--------------------------------------------------------------------------------
------------------------- Taxi Services : Star Schema --------------------------
--------------------------------------------------------------------------------

-- dimension table for date time
DROP TABLE IF EXISTS datetime CASCADE ;
CREATE TABLE datetime (
  datetime_id                   SERIAL NOT NULL,
  date_current                  TIMESTAMP NOT NULL,
  epoch                         BIGINT NOT NULL,
  hour                          SMALLINT NOT NULL,
  day_name                      VARCHAR(9) NOT NULL,
  day_abrv                      CHAR(3) NOT NULL,
  -- day_type: 'A' means normal day, 'B' if it is a day before an holiday, 'C' if it is an holiday
  day_type                      CHAR(1) NOT NULL,
  weekend_flag                  BOOLEAN NOT NULL,
  day_week                      SMALLINT NOT NULL,
  day_month                     SMALLINT NOT NULL,
  day_year                      SMALLINT NOT NULL,
  week_month                    SMALLINT NOT NULL,
  week_year                     SMALLINT NOT NULL,
  month_current                 SMALLINT NOT NULL,
  month_name                    VARCHAR(9) NOT NULL,
  month_name_abrv               CHAR(3) NOT NULL,
  quarter                       SMALLINT NOT NULL,
  quarter_name                  VARCHAR(6) NOT NULL,
  year_current                  SMALLINT NOT NULL,

  -- checks year, month, day, hour, week, quarter, ...
  CHECK (hour >= 0 AND hour < 24),
  CHECK (day_type IN ('A', 'B', 'C')),
  CHECK (day_week >= 0 AND day_week <= 6),
  CHECK (day_month >= 1 AND day_month <= 31),
  CHECK (day_year >= 1 AND day_year <= 366),
  CHECK (week_month >= 1 AND week_month <= 5),
  CHECK (week_year >= 1 AND week_year <= 53),
  CHECK (month_current >= 1 AND month_current <= 12),
  CHECK (quarter >= 1 AND quarter <= 4),
  CHECK (year_current >= 1970),

  -- set primary keys
  PRIMARY KEY (datetime_id)
);

-- datetime index on date_current
CREATE INDEX datetime_date_current_idx ON datetime(date_current);

-- datetime index on epoch
CREATE INDEX datetime_epoch_idx ON datetime(epoch);

-- dimension table for taxi
DROP TABLE IF EXISTS taxi CASCADE ;
CREATE TABLE taxi (
  taxi_id                       SERIAL NOT NULL,
  license_nr                    INT NOT NULL,

  -- checks license number
  CHECK (license_nr > 0),

  -- set primary keys
  PRIMARY KEY (taxi_id)
);

-- dimension table for taxi stands
DROP TABLE IF EXISTS stand CASCADE ;
CREATE TABLE stand (
  stand_id                      SERIAL NOT NULL,
  name                          VARCHAR(255),
  capacity                      INT,

  -- checks capacity
  CHECK (capacity > 0),

  -- set primary keys
  PRIMARY KEY (stand_id)
);

-- dimension table for location
DROP TABLE IF EXISTS location CASCADE ;
CREATE TABLE location (
  location_id                   SERIAL NOT NULL,
  freguesia                     VARCHAR(100),
  concelho                      VARCHAR(50),
  region                        GEOMETRY(MULTIPOLYGON, 4326),

  -- set primary keys
  PRIMARY KEY (location_id)
);

-- bridge table for stand dimension and stand location
DROP TABLE IF EXISTS stand_location CASCADE ;
CREATE TABLE stand_location (
  location_id                   INT NOT NULL,
  stand_id                      INT NOT NULL,

  -- set primary keys
  PRIMARY KEY (location_id, stand_id),

  -- set foreign keys
  FOREIGN KEY (location_id) REFERENCES location (location_id),
  FOREIGN KEY (stand_id) REFERENCES stand (stand_id)
);

-- fact table for taxi services
DROP TABLE IF EXISTS service CASCADE ;
CREATE TABLE service (
  taxi_id                       INT NOT NULL,
  datetime_init_id              INT NOT NULL,
  datetime_end_id               INT NOT NULL,
  local_init_id                 INT NOT NULL,
  local_end_id                  INT NOT NULL,
  no_trips                      SMALLINT NOT NULL,
  total_time                    INT NOT NULL,
  total_euclidean_kms           DOUBLE PRECISION NOT NULL,

  -- checks number of trips, total time, distance
  CHECK (no_trips > 0 AND total_time >= 0 AND service.total_euclidean_kms >= 0),

  -- set primary keys
  PRIMARY KEY (taxi_id, datetime_init_id, datetime_end_id, local_init_id, local_end_id),

  -- set foreign keys
  FOREIGN KEY (taxi_id) REFERENCES taxi (taxi_id),
  FOREIGN KEY (datetime_init_id) REFERENCES datetime (datetime_id),
  FOREIGN KEY (datetime_end_id) REFERENCES datetime (datetime_id),
  FOREIGN KEY (local_init_id) REFERENCES location (location_id),
  FOREIGN KEY (local_end_id) REFERENCES location (location_id)
);


