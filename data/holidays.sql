--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'SQL_ASCII';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: holidays; Type: TABLE; Schema: public; Owner: postgres; Tablespace:
--

DROP TABLE IF EXISTS holidays;
CREATE TABLE holidays (
		id integer NOT NULL,
		name character varying(255),
		date date NOT NULL
);


ALTER TABLE public.holidays OWNER TO postgres;

--
-- Data for Name: holidays; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY holidays (id, name, date) FROM stdin;
1	Ano Novo	'2015-01-01'
2	Carnaval	'2015-02-16'
3	Sexta-feira Santa	'2015-04-03'
4	Páscoa	'2015-04-05'
5	Segunda-feira de Páscoa	'2015-04-06'
6	Dia da Liberdade	'2015-04-25'
7	'Dia do Trabalhador'	'2015-05-01'
8	Corpo de Deus	'2015-06-04'
9	Dia de Portugal	'2015-06-10'
10	Dia de São João	'2015-06-24'
11	Assunção de Maria	'2015-08-15'
12	Implantação da República	'2015-10-05'
13	Todos os Santos	'2015-11-01'
14	Restauração da Independência	'2015-12-01'
15	Imaculada Conceição	'2015-12-08'
16	Natal	'2015-12-25'
\.


--
-- Name: holidays_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace:
--

ALTER TABLE ONLY holidays
		ADD CONSTRAINT holidays_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

