#!/usr/bin/env bash

######################################################
#         Import Data Warehouse model data           #
######################################################

# load environment
export $(cat .env | grep -v ^# | xargs)

IMPORT_FILE=import_taxi_services.sql

PGPASSWORD=${PGSQL_PASSWORD} psql -d ${PGSQL_DB} -h ${PGSQL_HOSTNAME} -U ${PGSQL_USER} -p ${PGSQL_PORT} -f ${IMPORT_FILE}
