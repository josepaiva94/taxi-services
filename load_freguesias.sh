#!/usr/bin/env bash

######################################################
# Loads portuguese freguesias to postgresql database #
######################################################

# load environment
export $(cat .env | grep -v ^# | xargs)

# change this to your data!
SHP_FILE=data/freguesias/Cont_Freg_V5.shp

shp2pgsql -s 3763:4326 -c -I -d -W "latin1" ${SHP_FILE} public.caop | \
PGPASSWORD=${PGSQL_PASSWORD} psql -d ${PGSQL_DB} -h ${PGSQL_HOSTNAME} -p ${PGSQL_PORT} -U ${PGSQL_USER}
