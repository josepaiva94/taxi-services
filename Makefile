
all: loadFreguesias loadData createModel importModelData

loadFreguesias:
			. ./load_freguesias.sh
			echo "Loaded freguesias!"

loadData:
			. ./load_data.sh
			echo "Loaded data!"

createModel:
			. ./create_model.sh
			echo "Created DW model!"

importModelData:
			. ./import_model_data.sh
			echo "Imported data into DW!"

