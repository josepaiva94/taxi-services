#!/usr/bin/env bash

######################################################
#           Create Data Warehouse model              #
######################################################

# load environment
export $(cat .env | grep -v ^# | xargs)

SCHEMA_FILE=taxi_services_schema.sql

PGPASSWORD=${PGSQL_PASSWORD} psql -d ${PGSQL_DB} -h ${PGSQL_HOSTNAME} -U ${PGSQL_USER} -p ${PGSQL_PORT} -f ${SCHEMA_FILE}
