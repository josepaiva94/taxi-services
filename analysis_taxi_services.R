
# Create a connection to the database
library('RPostgreSQL')
library('ggplot2')
library('dplyr')
library('reshape2')
library('xtable')

this.dir <- dirname(parent.frame(3)$ofile)
setwd(this.dir)

## Loading required package: DBI
pg <- dbDriver("PostgreSQL")

# Connect to database
con <- dbConnect(pg, user="postgres", password="tabdpg",
                 host="localhost", port=5432, dbname="postgres")

# query: number of trips started per day of the week
query <- "SELECT dt_init.day_name, sum(no_trips) FROM service LEFT JOIN datetime AS dt_init ON service.datetime_init_id = dt_init.datetime_id GROUP BY dt_init.day_name;"
data <- dbGetQuery(con, query)

ggplot(data, aes(day_name, sum)) +
  geom_bar(stat = "identity") +
  labs(x = "Day of the Week", y = "Number of Trips") 

# query: number of trips started per day of the week and type of day
query <- "SELECT dt_init.day_name, dt_init.day_type, sum(no_trips) FROM service LEFT JOIN datetime AS dt_init ON service.datetime_init_id = dt_init.datetime_id GROUP BY dt_init.day_name, dt_init.day_type;"
data <- dbGetQuery(con, query)

ggplot(data, aes(day_name, sum)) +
  geom_bar(stat = "identity", aes(fill = as.factor(data$day_type))) +
  labs(x = "Day of the Week", y = "Number of Trips") +
  scale_fill_manual(name = "Type of Day",
                    labels = levels(as.factor(data$day_type)), 
                    values = c("dark_grey", "green", "blue"))

# query: number of trips started per day of the week and type of day with totals
query <- "SELECT dt_init.day_abrv, dt_init.day_type, sum(no_trips) FROM service LEFT JOIN datetime AS dt_init ON service.datetime_init_id = dt_init.datetime_id GROUP BY CUBE(dt_init.day_abrv, dt_init.day_type);"
data <- as.data.frame(dbGetQuery(con, query))
data$day_abrv[is.na(data$day_abrv)] <- "Total"
data$day_type[is.na(data$day_type)] <- "Total"
data %>%
  melt(id.vars = c('day_abrv', 'day_type')) %>%
  dcast(day_type ~ day_abrv + variable) %>%
  select(day_type, Mon_sum, Tue_sum, Wed_sum, Thu_sum, Fri_sum, Sat_sum, Sun_sum, Total_sum) ->
  data
colnames(data) <- c("Type of Day", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun", "Total")

write.csv(data, file = "./results/query3.csv", sep = "\t")
write(print(xtable(data), align = "l|rrrrrrr"), file = "results/query3-latex.txt")

# query: average number of trips started per day of the week and type of day with totals
query <- "SELECT day_abrv, day_type, avg(sum.no_trips) FROM (SELECT day_abrv, day_type, sum(no_trips) AS no_trips FROM service LEFT JOIN datetime AS dt_init ON service.datetime_init_id = dt_init.datetime_id GROUP BY day_month, month_current, year_current, day_abrv, day_type) AS sum GROUP BY CUBE(day_abrv, day_type);"
data <- as.data.frame(dbGetQuery(con, query))
data$day_abrv[is.na(data$day_abrv)] <- "Total"
data$day_type[is.na(data$day_type)] <- "Total"
data %>%
  melt(id.vars = c('day_abrv', 'day_type')) %>%
  dcast(day_type ~ day_abrv + variable) %>%
  select(day_type, Mon_avg, Tue_avg, Wed_avg, Thu_avg, Fri_avg, Sat_avg, Sun_avg, Total_avg) ->
  data
colnames(data) <- c("Type of Day", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun", "Total")

write.csv(data, file = "results/query4.csv")
write(print(xtable(data), align = "l|rrrrrrr"), file = "results/query4-latex.txt")

# query: max services within location


# query: location of max services


# query: given a local x 



# Disconnect from the database
dbDisconnect(con)

