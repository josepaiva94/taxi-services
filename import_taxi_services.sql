
--------------------------------------------------------------------------------
------------------------- Taxi Services : Import Data --------------------------
--------------------------------------------------------------------------------

-- clean tables
DELETE FROM service;
DELETE FROM datetime;
DELETE FROM taxi;
DELETE FROM stand_location;
DELETE FROM location;
DELETE FROM stand;

-- insert datetime data into datetime dimension table
INSERT INTO datetime(date_current, epoch, hour, day_name, day_abrv, day_type, weekend_flag, day_week, day_month,
                     day_year, week_month, week_year, month_current, month_name, month_name_abrv, quarter, quarter_name,
                     year_current)
  SELECT
    datum AS date_current,
    extract(EPOCH FROM datum) AS epoch,
    extract(HOUR FROM datum) AS hour,
    to_char(datum, 'Day') AS day_name,
    to_char(datum, 'Dy') AS day_abrv,
    (CASE
      WHEN datum::DATE IN (SELECT date FROM holidays) THEN 'C'
      WHEN datum::DATE IN (SELECT date - '1 day'::INTERVAL FROM holidays) THEN 'B'
      ELSE 'A'
    END) AS day_type,
    (CASE
      WHEN extract(DOW FROM datum) IN (0, 6) THEN TRUE
      ELSE FALSE
    END) AS weekend_flag,
    extract(DOW FROM datum) AS day_week,
    extract(DAY FROM datum) AS day_month,
    extract(DOY FROM datum) AS day_year,
    to_char(datum, 'W')::INT AS week_month,
    extract(WEEK FROM datum) AS week_year,
    extract(MONTH FROM datum) AS month_current,
    to_char(datum, 'Month') AS month_name,
    to_char(datum, 'Mon') AS month_name_abrv,
    extract(QUARTER FROM datum) AS quarter,
    CASE
      WHEN extract(QUARTER FROM datum) = 1 THEN 'First'
      WHEN extract(QUARTER FROM datum) = 2 THEN 'Second'
      WHEN extract(QUARTER FROM datum) = 3 THEN 'Third'
      WHEN extract(QUARTER FROM datum) = 4 THEN 'Fourth'
    END AS quarter_name,
    extract(YEAR FROM datum) AS year
  FROM (
    SELECT to_timestamp(datum_start)::DATE + generate_series(0, (datum_stop - datum_start)/3600 + 1) * '1 hour'::INTERVAL AS datum
    FROM
      (SELECT initial_ts AS datum_start FROM taxi_services ORDER BY initial_ts ASC LIMIT 1) AS datum_start,
      (SELECT final_ts AS datum_stop FROM taxi_services ORDER BY final_ts DESC LIMIT 1) AS datum_stop
  ) AS datum;

-- insert taxis data into taxi dimension table
INSERT INTO taxi(license_nr)
  SELECT DISTINCT taxi_id FROM taxi_services ORDER BY 1 ASC;

-- insert taxi stands into stand dimension table
INSERT INTO stand(stand_id, name, capacity)
  SELECT id, name, 5
  FROM taxi_stands;

-- insert location of stands into location dimension table
INSERT INTO location(freguesia, concelho, region)
  SELECT DISTINCT
    freguesia AS freguesia,
    concelho AS concelho,
    geom AS region
  FROM (stand LEFT JOIN taxi_stands ON stand_id = taxi_stands.id) LEFT JOIN caop ON st_contains(geom, location);

-- insert stand location data into stand_location bridge table
INSERT INTO stand_location(location_id, stand_id)
  SELECT location_id, stand_id
  FROM (stand LEFT JOIN taxi_stands ON stand_id = id) LEFT JOIN location ON st_contains(region, taxi_stands.location);

-- insert services data into services fact table
INSERT INTO service(taxi_id, datetime_init_id, datetime_end_id, local_init_id, local_end_id, no_trips, total_time,
                    total_euclidean_kms)
  SELECT
    taxi.taxi_id AS taxi_id,
    dt_init.datetime_id AS datetime_init_id,
    dt_end.datetime_id AS datetime_end_id,
    loc_init.location_id AS local_init_id,
    loc_end.location_id AS local_end_id,
    count(*) AS no_trips,
    sum(extract(EPOCH FROM to_timestamp(taxi_services.final_ts) - to_timestamp(taxi_services.initial_ts))) AS total_time,
    sum(st_distance(st_transform(taxi_services.initial_point, 3763),
                    st_transform(taxi_services.final_point, 3763)) / 1000) AS total_euclidean_kms
  FROM
    taxi_services, taxi, datetime AS dt_init, datetime AS dt_end, location AS loc_init, location AS loc_end
  WHERE
    taxi_services.taxi_id = taxi.license_nr AND
    date_trunc('hour', to_timestamp(taxi_services.initial_ts)) = dt_init.date_current AND
    date_trunc('hour', to_timestamp(taxi_services.final_ts)) = dt_end.date_current AND
    st_contains(loc_init.region, taxi_services.initial_point) AND
    st_contains(loc_end.region, taxi_services.final_point)
  GROUP BY
    taxi.taxi_id, dt_init.datetime_id, dt_end.datetime_id, loc_init.location_id, loc_end.location_id;


