#!/usr/bin/env bash

######################################################
# Loads taxi and taxi stands to postgresql database  #
######################################################

# load environment
export $(cat .env | grep -v ^# | xargs)

# change this to your data!
DATA_DIR=data

for file in ${DATA_DIR}/*.sql
    do PGPASSWORD=${PGSQL_PASSWORD} psql -d ${PGSQL_DB} -h ${PGSQL_HOSTNAME} -p ${PGSQL_PORT} -U ${PGSQL_USER} -f ${file}
done
